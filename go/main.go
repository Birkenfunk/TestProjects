package main

import (
	"fmt"
)

type address struct {
	ID       int32  `json:"id"`
	Country  string `json:"country"`
	City     string `json:"city"`
	Street   string `json:"street"`
	Postcode string `json:"postcode"`
}

var addresses = []address{
	{1, "Germany", "Berlin", "Kurfürstendamm", "10719"},
	{2, "Germany", "Berlin", "Unter den Linden", "10117"},
	{3, "Germany", "Berlin", "Alexanderplatz", "10178"},
	{4, "Germany", "Munich", "Marienplatz", "80331"},
	{5, "Germany", "Munich", "Odeonsplatz", "80538"},
	{6, "Austria", "Winna", "Stephansplatz", "1010"},
	{7, "Austria", "Winna", "Kaiserplatz", "1010"},
	{8, "Austria", "Winna", "Königsplatz", "1010"},
	{9, "Switzerland", "Zürich", "Bahnhofplatz", "8001"},
	{10, "Switzerland", "Zürich", "Bahnhofstrasse", "8001"},
	{11, "France", "Paris", "Champs-Élysées", "75008"},
}

func main() {
	fmt.Printf("Hello World")
}
